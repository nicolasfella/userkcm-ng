/*
    Copyright 2016-2018 Jan Grulich <jgrulich@redhat.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "kcm.h"

// KDE
#include <KAboutData>
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QFileDialog>
#include <QTimer>
#include <QtQuick/QQuickItem>

#include "user.h"
#include "usermodel.h"
#include "usercontroller.h"
#include "kdeconnectpicturechooser.h"

Q_LOGGING_CATEGORY(KCM_USER, "kcm_user")

K_PLUGIN_FACTORY_WITH_JSON(KCMUserFactory, "kcm_user.json", registerPlugin<KCMUser>();)


KCMUser::KCMUser(QObject *parent, const QVariantList &args)
    : KQuickAddons::ConfigModule(parent, args)
{
    KAboutData* about = new KAboutData(QStringLiteral("kcm_user"), i18n("Manage user accounts"),
                                       QStringLiteral("0.1"), QString(), KAboutLicense::GPL);
    about->addAuthor(i18n("Nicolas Fella"), QString(), QStringLiteral("nicolas.fella@gmx.de"));
    setAboutData(about);
    setButtons(Apply);

    qmlRegisterType<UserModel>("org.kde.userng", 1, 0, "UserModel");
    qmlRegisterType<UserController>("org.kde.userng", 1, 0, "UserController");
    qmlRegisterType<KdeConnectPictureChooser>("org.kde.userng", 1, 0, "KdeConnectPictureChooser");
}

KCMUser::~KCMUser()
{
}

void KCMUser::defaults()
{
    KQuickAddons::ConfigModule::defaults();
}

void KCMUser::load()
{
    KQuickAddons::ConfigModule::load();
}

void KCMUser::save()
{
    KQuickAddons::ConfigModule::save();
    Q_EMIT apply();
}

#include "kcm.moc"
