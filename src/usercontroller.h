#pragma once

#include <QObject>

class OrgFreedesktopAccountsInterface;

class UserController : public QObject
{

    Q_OBJECT

public:
    explicit UserController();
    virtual ~UserController() = default;

    Q_SCRIPTABLE void createUser(const QString& name);
    Q_SCRIPTABLE void deleteUser(int index);

private:
    OrgFreedesktopAccountsInterface* m_dbusInterface;
};
