/**
 * Copyright 209 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "usermodel.h"

#include <QDebug>
#include <QDBusPendingReply>
#include <QDBusInterface>

#include "accounts_interface.h"

UserModel::UserModel(QObject* parent)
    : QAbstractListModel(parent)
    , m_dbusInterface(nullptr)
{

    connect(this, &QAbstractItemModel::rowsInserted,
            this, &UserModel::rowsChanged);
    connect(this, &QAbstractItemModel::rowsRemoved,
            this, &UserModel::rowsChanged);

    m_dbusInterface = new OrgFreedesktopAccountsInterface(QStringLiteral("org.freedesktop.Accounts"), QStringLiteral("/org/freedesktop/Accounts"), QDBusConnection::systemBus(), this);

    connect(m_dbusInterface, &OrgFreedesktopAccountsInterface::UserAdded, this, [this](const QDBusObjectPath &path) {
        User *user = new User();
        user->setPath(path);
        beginInsertRows(QModelIndex(), m_userList.size(), m_userList.size());
        m_userList.append(user);
        endInsertRows();
    });

    connect(m_dbusInterface, &OrgFreedesktopAccountsInterface::UserDeleted, this, [this](const QDBusObjectPath &path) {
        for (User* user: m_userList) {
            if (user->path() == path) {
                int index = m_userList.indexOf(user);
                beginRemoveRows(QModelIndex(), index, index);
                delete user;
                m_userList.removeAt(index);
                endRemoveRows();
            }
        }
    });

    QDBusPendingReply <QList <QDBusObjectPath > > reply = m_dbusInterface->ListCachedUsers();
    reply.waitForFinished();

    if (reply.isError()) {
        qDebug() << reply.error().message();
        return;
    }

    const QList<QDBusObjectPath> users = reply.value();
    for (const QDBusObjectPath& path: users) {
        qDebug() << "User found";
        User *user = new User();
        user->setPath(path);
        m_userList.append(user);
    }
}

QHash<int, QByteArray> UserModel::roleNames() const
{
    QHash<int, QByteArray> names = QAbstractItemModel::roleNames();
    names.insert(UidRole, "uid");
    names.insert(NameRole, "name");
    names.insert(RealNameRole, "realName");
    names.insert(EmailRole, "email");
    names.insert(FaceRole, "face");
    names.insert(AutoLoginRole, "autoLogin");
    names.insert(AdministratorRole, "administrator");
    names.insert(UserRole, "User");
    return names;
}

UserModel::~UserModel()
{
}

QVariant UserModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()
        || index.row() < 0
        || index.row() >= m_userList.count())
    {
        return QVariant();
    }

    User *user = m_userList[index.row()];

    switch (role) {
        case NameRole:
            return user->name();
        case FaceRole:
            return user->face();
        case RealNameRole:
            return user->realName();
        case EmailRole:
            return user->email();
        case AdministratorRole:
            return user->administrator();
        case AutoLoginRole:
            return user->autologin();
        case UserRole:
            return QVariant::fromValue(user);
        default:
             return QVariant();
    }
}

int UserModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid()) {
        //Return size 0 if we are a child because this is not a tree
        return 0;
    }

    return m_userList.count();
}
