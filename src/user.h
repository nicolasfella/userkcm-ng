#ifndef USER_H
#define USER_H

#include <QObject>
#include <QDBusObjectPath>

class OrgFreedesktopAccountsUserInterface;

class User : public QObject {
    Q_OBJECT

    Q_PROPERTY(int uid READ uid
            WRITE setUid
            NOTIFY uidChanged
    )

    Q_PROPERTY(QString name READ name
            WRITE setName
            NOTIFY nameChanged
    )

    Q_PROPERTY(QString realName READ realName
            WRITE setRealName
            NOTIFY realNameChanged
    )

    Q_PROPERTY(QString email READ email
            WRITE setEmail
            NOTIFY emailChanged
    )

    Q_PROPERTY(QString face READ face
            WRITE setFace
            NOTIFY faceChanged
    )

    Q_PROPERTY(bool autologin READ autologin
            WRITE setAutologin
            NOTIFY autologinChanged
    )

    Q_PROPERTY(bool administrator READ administrator
            WRITE setAdministrator
            NOTIFY administratorChanged
    )

public:
    explicit User(QObject* parent = nullptr);


     int uid() const;
     void setUid(int value);

     QString name() const;
     void setName(const QString& value);

     QString realName() const;
     void setRealName(const QString& value);

     QString email() const;
     void setEmail(const QString& value);

     QString face() const;
     void setFace(const QString& value);

     bool autologin() const;
     void setAutologin(bool value);

     bool administrator() const;
     void setAdministrator(bool value);

     QDBusObjectPath path() const;
     void setPath(const QDBusObjectPath &path);

public Q_SLOTS:
    Q_SCRIPTABLE void apply();

signals:

    void uidChanged(int uid);

    void nameChanged(const QString& name);

    void realNameChanged(const QString& realName);

    void emailChanged(const QString& email);

    void faceChanged(const QString& face);

    void autologinChanged(bool autologin);

    void administratorChanged(bool administrator);

private:
    int mUid;
    QString mName;
    QString mRealName;
    QString mEmail;
    QString mFace;
    bool mAutologin;
    bool mAdministrator;
    QDBusObjectPath mPath;
    OrgFreedesktopAccountsUserInterface *m_dbusIface;
};

#endif // USER_H
