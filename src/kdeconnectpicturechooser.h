#pragma once
#include <QObject>

class KdeConnectPictureChooser : public QObject
{
    Q_OBJECT

public:
    explicit KdeConnectPictureChooser();
    virtual ~KdeConnectPictureChooser() = default;

Q_SIGNALS:
    Q_SCRIPTABLE void pictureAvailable(const QString& fileName);

public Q_SLOTS:
    void requestPicture();
    void rec(const QString& fileName);
};
