#include "usercontroller.h"

#include <QDBusConnection>

#include "accounts_interface.h"

UserController::UserController()
    : QObject()
{
    m_dbusInterface = new OrgFreedesktopAccountsInterface(QStringLiteral("org.freedesktop.Accounts"), QStringLiteral("/org/freedesktop/Accounts"), QDBusConnection::systemBus(), this);
}

void UserController::createUser(const QString& name)
{
    m_dbusInterface->CreateUser(name, QString(), 0);
}

void UserController::deleteUser(int id)
{
    m_dbusInterface->DeleteUser(id, false);
}

#include "usercontroller.moc"
